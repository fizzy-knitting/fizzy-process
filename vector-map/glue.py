#!/usr/bin/env python3

import json
import csv
import datetime

DATA = []
TAGS = []

source_files = []

for gtype in ['points', 'lines', 'multilinestrings', 'multipolygons', 'other_relations']:
    src_fn = 'great-britain-voltage-{}.json'.format(gtype)
    with open(src_fn, 'r', encoding='utf-8') as fin:
        source_files.append(src_fn)
        data = json.load(fin)
        DATA.append(data)
        TAGS += [{k: v for (k, v) in i['properties'].items()
                 if v and k not in ['z_order', 'layer']}
                for i in data['features'] if i]


with open('output-all.json', 'w') as fout:
    json.dump(DATA, fout)

with open('tag-report.csv', 'w') as fout:
    fieldnames = ['osm_id', 'electrified', 'frequency', 'voltage', 'railway']
    fieldnames += [k for k in set([j for i in TAGS for j in i]) - set(fieldnames)]
    csvwriter = csv.DictWriter(fout, fieldnames=fieldnames)
    csvwriter.writeheader()
    for tag in TAGS:
        csvwriter.writerow(tag)


md = "# Latest Feed, Files & Data\n\n"

md += "**%s**\n\n" % datetime.datetime.strftime(datetime.datetime.now(), "%c")


md += "- GeoJSON data [/latest/output-all.json](/latest/output-all.json)\n"
md += "- OSM tags.csv [/latest/tag-report.csv](/latest/tag-report.csv)\n"

md += "\n## Splitted Sources\n"

for fn in source_files:
    md += "- [/latest/%s](/latest/%s)\n" % (fn, fn)

with open('index.md', 'w') as fout:
    fout.write(md)


