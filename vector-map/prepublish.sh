#!/bin/sh

CONTINENT=europe
REGION=great-britain

URL=http://download.geofabrik.de/${CONTINENT}
FILENAME=${REGION}-latest.osm.pbf
if [ ! -s  ${FILENAME} ]; then
    curl -L -o ${FILENAME} ${URL}/${FILENAME}
fi

if [ ! -f ${REGION}.poly ]; then
    echo missing ${REGION}.poly polygon file
    exit 1
fi

if [ x"$(find ${FILENAME} -mmin -15)" = x ]; then
    osmupdate ${FILENAME} ${REGION}-update.osm.pbf -B=${REGION}.poly --verbose --keep-tempfiles
    mv ${REGION}-update.osm.pbf ${FILENAME}
else
    echo "${FILENAME} is less than 15 minutes old"
fi

for element in points lines multilinestrings multipolygons other_relations
do
    KEYWORD=voltage
    ogr2ogr --config OGR_INTERLEAVED_READING YES --config OSM_CONFIG_FILE osmconf-${KEYWORD}.ini \
            -where "railway='rail' OR railway='subway' OR railway='light_rail' OR railway='tram' OR railway='narrow_gauge' OR (railway IS NOT NULL AND voltage IS NOT NULL)" \
		        -f GeoJSON ${REGION}-${KEYWORD}-${element}.json \
		        ${FILENAME} ${element}
done

./glue.py
