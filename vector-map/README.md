# Fizzy Knitting vector-map/

- From https://gist.github.com/anisotropi4/757430340067ebfdd144bdd3ce92823c
- and chat at https://gitter.im/fizzy-knitting/community

## Overview

Open Street Map contains lots of data about the railway in 
mainland Britain. These 
scripts extract the full Open Street Map dataset and then 
filter for point associated with rail.


## Dependancies

Install the required OSM update and GDAL dependencies for the `ogr2ogr` tool (and `python3`)

```bash
sudo apt install osmctools gdal-bin python3
```

## prepublish.sh Process


So the [prepublish.sh](https://gitlab.com/fizzy-knitting/data/-/blob/master/prepublish.sh) script 
does three things:

## First download latest from OSM

- Where `FILENAME=great-britain-latest.osm.pbf`
- and `URL=http://download.geofabrik.de/europe`

Hopefully it only does this once as the `great-britain-latest.osm.pbf` is big (1119MB)

```bash
curl -L -o ${FILENAME} ${URL}/${FILENAME}
```


## Second step and also second time update

The next thing it does is to use the `osmupdate` executable from 
the `osmscutils` to update the `great-britain-latest.osm.pbf` file

Which takes a bit of time depending on how far out the update to 
create a `great-britain-update.osm.pbf` file.

## Magic Ah!.a

GeoJSON consists of five geometry types

 - points
 - lines
 - multilinestrings
 - multipolygons 
 - other_relations

What the `ogr2ogr` script does is to filter the `great-britain-update.osm.pbf` file
based on the "query"

```sql
"railway='rail' OR railway='subway' OR railway='light_rail' OR railway='tram' OR railway='narrow_gauge' OR (railway IS NOT NULL AND voltage IS NOT NULL)"
```
Looks for these geometry types in the OSM .pbf file for the five geometry types
and dumps them into five files called

- `great-britain-voltage-lines.json`
- `great-britain-voltage-multilinestrings.json`
- `great-britain-voltage-multipolygons.json`
- `great-britain-voltage-other_relations.json`
- `great-britain-voltage-points.json`

## glue.py

The final step is to run the `glue.py` script that shunts all these files 
into a final `output-all.json` file which is rendered

ALSO:

- creates a CSV report file `tag-report.csv` with all the tags from the GeoJSON in a report


