fizzy-process
======================


This is code that generates map and data and in general it
pulls the latest upstream OSM data then some processing for output

There are two key projects and associated directories.

## tile-map/

This is Martin Hoffman ala @partim expertise:

- Pulls OSM data
- populates as postgis database
- mapnik to serve `map` tiles


## vector-map/
 
This is Will Deakin ala @anisotropi4 exoertise:

- Pulls OSM data
- does processing
- outputs `geojson` files

