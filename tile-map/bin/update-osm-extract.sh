#! /bin/bash
#
# Updates an OSM extract.
#
# Continoulsy fetches the OSM database updates for a certain extract waiting
# a minute between updates. Filters and imports the extract into a PostGIS
# database once every hour.

EXTRACT_URL=http://download.openstreetmap.fr/extracts/europe/united_kingdom-latest.osm.pbf
REPL_URL=http://download.openstreetmap.fr/replication/europe/united_kingdom/minute/
EXTRACT_FILE=/var/cache/openstreetmap/extract.osm.pbf

while true; do
    # Get the extract if it isn't there.
    if [ ! -r "$EXTRACT_FILE" ]; then
        wget -O "$EXTRACT_FILE" "$EXTRACT_URL"
        pyosmium-up-to-date --ignore-osmosis-headers --server "$REPL_URL" "$EXTRACT_FILE"
    fi

    # Get all changes.
    status=1
    while [ $status -eq 1 ]; do
        pyosmium-up-to-date "$EXTRACT_FILE"
        status=$?
    done
    sleep 60 
done

