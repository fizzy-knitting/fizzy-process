#! /bin/bash
#
# Updates an OSM database.
#
# Because of concurrent database access, we need to shut down the map
# renderer. So this needs to be run as root.

EXTRACT_FILE=/var/cache/openstreetmap/extract.osm.pbf
FILTERED_FILE=/var/cache/openstreetmap/extract.filtered.osm.pbf
SCRIPT_DIR="$(dirname $(readlink -f $0))"
TAGS_FILE="$SCRIPT_DIR/../conf/tags-filter"
DB_STYLE="$SCRIPT_DIR/../conf/osm2pgsql.style"
DB_NAME="osm"

systemctl stop fizzy-map
osmium tags-filter "$EXTRACT_FILE" -o "$FILTERED_FILE" \
   --overwrite -e "$TAGS_FILE"
osm2pgsql --slim --drop --database "$DB_NAME" \
   "$FILTERED_FILE" --style "$DB_STYLE"
systemctl start fizzy-map

