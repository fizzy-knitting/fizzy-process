#! /bin/bash
#
# Prepares the system for the OSM database abstract.
#
# Should be run as root.

EXTRACT_USER=basemap
EXTRACT_DB=osm

sudo -u postgres createdb --encoding=UTF8 --owner "$EXTRACT_USER" "$EXTRACT_DB"
sudo -u postgres psql -d "$EXTRACT_DB" -c 'CREATE EXTENSION postgis; CREATE EXTENSION hstore;'
mkdir /var/cache/openstreetmap
chown $EXTRACT_USER:root /var/cache/openstreetmap

